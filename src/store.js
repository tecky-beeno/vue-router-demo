import { createStore } from 'vuex'

const store = createStore({
  state() {
    return {
      selectedDistrictId: null,
    }
  },
  mutations: {
    selectDistrict(state, district_id) {
      state.selectedDistrictId = district_id
    },
  },
})

export default store
