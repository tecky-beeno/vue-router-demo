import { createRouter, createWebHistory } from 'vue-router'
import SelectDistrictView from '../views/SelectDistrictView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'SelectDistrict',
      component: SelectDistrictView
    },
    {
      path: '/district/:id',
      name: 'SelectLibrary',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/SelectLibraryView.vue')
    }
  ]
})

export default router
