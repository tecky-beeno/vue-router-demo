# vue-router-demo

This project demo how to use vue router to pass parameter from previous page

## Demo Files
- [router/index.js](./src/router/index.js)
- [main.js](./src/main.js) (`app.use(router)`)
- [views/SelectDistrictView](./src/views/SelectDistrictView.vue)
- [views/SelectLibraryView](./src/views/SelectLibraryView.vue)

## Getting Started
This project is created with `npm init vite`, you can refer to [toolkit.md](./toolkit.md)
